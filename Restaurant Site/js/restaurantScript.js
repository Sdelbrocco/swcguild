function checkContactUsForm(){
	var name = document.getElementById("name").value;
	var email = document.getElementById("email").value;
	var phone = document.getElementById("phone").value;
	var reason = document.getElementById("reason").value;
	var additionalInfo = document.getElementById("additionalInfo").innerHTML;
	var monday = document.getElementById("monday");
	var tuesday = document.getElementById("tuesday");
	var wednesday = document.getElementById("wednesday");
	var thursday = document.getElementById("thursday");
	var friday = document.getElementById("friday");

	if (name == ""){
		alert("Don't forget to add your name!");
		return false;
	}else if (email == ""){
		alert("Don't forget to provide your email!");
		return false;
	}else if (reason == "Other" && additionalInfo == ""){
		alert("Please provide other reasoning in Additional Information section");
		return false;
	}
	if(monday.checked == false && 
		tuesday.checked == false && 
		wednesday.checked == false && 
		thursday.checked == false &&
		friday.checked == false){
		alert("Please provide best days to contact you");
		return false;
	}
	return true;


}
window.checkContactUsForm = checkContactUsForm;